#!/usr/bin/perl -w

use strict;
use warnings;
use JSON;

## Description: This script prepares to load data to G4R; massages the data to fit into the G4R tables
## Parameters: [0] - GenomeStudio matrix
##	       [1] - Samplesheet
##	       [2] - SNPmap

my %snp_genotype;
my %snp_markers;
my %stocks;
my @samples;

########## Initialize all Hash; Store values to respective hashes ##########
init_snp_markers($ARGV[2]);
init_stocks($ARGV[1]);
init_genotypes($ARGV[0]);
############################################################################

########## Output Routines ##########
#printMarkers();
printStocks();
#printGenotypes();
############################################################################


#initialize genotype_file
sub init_genotypes{
	my ($genotypefile) =@_;
	my $linecount = 0;
	my $skip = 9;
	
	open(GENO,"<",$genotypefile) || die "Cannot open $genotypefile:$!\n";
	while(my $line = <GENO>){
		chomp $line;
		if($linecount == $skip){
			$line =~ s/^\s|\s$//g;
			@samples = split(/\t/,$line);
		}elsif($linecount > $skip){
			my ($snp, $genotypes) = split(/\t/,$line,2);
			$genotypes =~ s/\s$//g;
			$snp_genotype{$snp} = [split(/\t/,$genotypes)];
		}
		$linecount++;
	}
	close(GENO);
	print "Genotype initialized.\n";
}

sub printGenotypes{
	
	foreach my $snp(sort keys %snp_genotype){
		foreach my $i(0..$#{$snp_genotype{$snp}}){
			my $allele = changeToIUPAC($snp_genotype{$snp}[$i]);
			print $snp."\t".$snp_markers{$snp}[0]."\t".$snp_markers{$snp}[1]."\t".$snp_markers{$snp}[2]."-".$snp_markers{$snp}[3]."\t".$samples[$i]."\t".$stocks{$samples[$i]}[10]."\t".$allele."\n";
		}
	}

}

## change bi-allelic allele notation to IUPAC mono-allelic format
sub changeToIUPAC{
	my ($makeIUPAC) = @_;

	if($makeIUPAC eq "AA"){
		return "A";
	}elsif($makeIUPAC eq "CC"){
		return "C";
	}elsif($makeIUPAC eq "GG"){
		return "G";
	}elsif($makeIUPAC eq "TT"){
		return "T";
	}elsif($makeIUPAC eq "AG" or $makeIUPAC eq "GA"){
		return "R";
	}elsif($makeIUPAC eq "CT" or $makeIUPAC eq "TC"){
		return "Y";
	}elsif($makeIUPAC eq "CA" or $makeIUPAC eq "AC"){
		return "M";
	}elsif($makeIUPAC eq "TG" or $makeIUPAC eq "GT"){
		return "K";
	}elsif($makeIUPAC eq "TA" or $makeIUPAC eq "AT"){
		return "W";
	}elsif($makeIUPAC eq "CG" or $makeIUPAC eq "GC"){
		return "S";
	}elsif($makeIUPAC eq "--"){
		return "N";
	}else{
		return "N";
	}
}

sub init_stocks{
	my ($samplesheet) = @_;
	my $linecount = 0;
	my $headerlines = 15;
	my $count = 1;
	
	#######################################################################
	# The Samplesheet table contains the following:
	#	[Header]
	#	[Data]
	#######################################################################
	open(STOCKS, "<", $samplesheet) || die "Cannot open $samplesheet:$!";
	while(my $line = <STOCKS>){
		chomp $line;
		if($linecount > $headerlines){
			my ($sample_id, $therest) = split(/\t/,$line,2);
			$therest =~ s/\s$//g;
			$stocks{$sample_id} = [split(/\t/,$therest)];
		}
		$linecount++;
	}
	close(STOCKS);
	print "stocks initialized.\n";
}



sub init_snp_markers{
	my ($markerfile)=@_;
	my $count = 0; 
	#######################################################################
	# The SNP Map table contains the following columns respectively
	#	SNPname
	#	Chromosome
	#	Position
	#	RefAllele
	#	AltAllele
	#######################################################################
	
	open(MARKER,"<",$markerfile) || die "Cannot open $markerfile:$!\n"; 
	while(my $line = <MARKER>){
		chomp $line;
		# skip the comments
		if($line !~ m/^#/){
			$count = $count+1;
			my ($snpname,$chr,$pos,$refallele,$altallele) = split(/\t/,$line);
			$altallele =~ s/^\s|\s$//g;
			$snp_markers{$snpname} = [$chr,$pos,$refallele,$altallele];
		}
	}	
	close(MARKER);
	print "SNP markers initialized.\n";
}


##change this-- no more snp_num
sub printMarkers{
	open(SNPF,">","snp_feature.tsv");
	open(SNPFL,">","snp_featureloc.tsv");
	open(SNPFP,">","snp_featureprop.tsv");
	# loop through the %snp_markers key; sort numerically
	foreach my $snp(sort  keys %snp_markers){ l
		print SNPF $snp."\t185\t".$snp_markers{$snp}[2]."\t".$snp_markers{$snp}[3]."\t54447\n";
		print SNPFL $snp."\t".$snp_markers{$snp}[0]."\t".$snp_markers{$snp}[1]."\n";
		print SNPFP $snp."\t54450\tinfinium6k\n";

		#foreach my $snp(keys %{$snp_markers{$snp_num}}){
		#	#print the id_num, snp_name and the Ref Allele
		#	print $snp_num."\t".$snp."\t".$snp_markers{$snp_num}{$snp}[3]."\n";
		#}
	}
	close(SNPF);
	close(SNPFL);
	close(SNPFP);
}

sub printStocks{
	open(ST_OUT,">","iric_stock.tsv");
	open(STP_OUT,">","iric_stockprop.tsv");
	foreach my $stock(sort keys %stocks){
		print ST_OUT "9\t42248\t".$stocks{$stock}[0]."\t".$stock."\n";
		#print STP_OUT $stock."\t54449\n";
		print STP_OUT $stock."\t54448\t".$stocks{$stock}[1]."\n";
		#print STP_OUT $stock."\t43780\t".$stocks{$stock}[2]."\n";
		print STP_OUT $stock."\t43613\t".$stocks{$stock}[3]."\n";

		#foreach my $i(0..$#{$stocks{$stock}})
		#	#print the entry_num, stock_id, the stock_name and stock_gid
		#	
		#}

	}
	close(ST_OUT);
}

