#!/usr/bin/perl

## Author: Venice Margarette B. Juanillas
## Date: January 19, 2016
## Description: This script prepares to load data to G4R; massages the data to fit into the G4R tables
## Parameters: [0] - GenomeStudio matrix
##	       [1] - Samplesheet
##	       [2] - SNPmap
############### Optional: must be script-generated ##########
##	       [3] - snp_feature-data
##	       [4] - snp_featureprop-data
##	       [5] - iric_stock-data
##	       [6] - iric_stockprop-data
##	       [7] - snp_genotype-data
##	       [6] - snp_genotypeprop-data



use strict;
use warnings;
use JSON;
use DBI;
use DBD::Oracle;
use Spreadsheet::ParseExcel;

my @samples; #
my %snpMatrix; # hash of array: key is snp_id; values are the alleles the order of which folloews index of elements in @samples
my %genotypesJSON;
my %stock; # hash of array: key is sample_id; values are the sample properties stated in samplesheet
my %snps; #

########### Parsing Routines ########### 
parseSampleSheet($ARGV[1]);
processGenomeStudio($ARGV[0]);
########################################

########### Output Routines ###########
#outputForDBLoad();

#
#### Test####
#printStocks();
#printOutputText();
printOutputJSON();  ## "massage" data to fit genotype table in g4r_gobii
########################################


########### Experimental ###########
#processExcelSheet($ARGV[0]);
#connectToOracle();
###########################################

## create data tables for loading to Oracle
sub outputForDBLoad{
	######### STOCK #########
	#create file for stock_table
	open(STOCK,">","add_stocks.tsv");

	# output: sample_id - sample_id2
	print STOCK "#dbxref_id\torganism_id\ttype_id\tname\tiric_stockgenolocation\n";
	my $count = 1;
	my $dbxref_id = '';
	foreach my $sample(@samples){
		print STOCK $dbxref_id."\t9\t42248\t".$sample."\t\t\n"; 
	}

	close(STOCK);
	######### End of STOCK #########


	######### SNP_GENOTYPE #########
	# create file for snp_genotype table
	open(SNPGeno,">","snp_genotype.tsv");	

	foreach my $marker(sort keys %snpMatrix){
		foreach my $x(0..$#{$snpMatrix{$marker}}){
			my $allele = changeToIUPAC($snpMatrix{$marker}[$x]);

			# Outputs: marker | sample_id |allele
			#print SNPGeno $marker."\t".$samples[$x]."\t".$allele."\n"; 
			
			# Alternate output: marker_id | sample_id2 | allele 
			#print SNPGeno $marker."\t".$stock{$samples[$x]}[0]."\t".$allele."\n";
		}
	}	
	close(SNPGeno);
	######### End of SNP_GENOTYPE #########
	
	######### SNP_FEATURE #########
	#create file for snp_feature
	open(SNPF,">","snp_fetaure.tsv");
	
	close(SNPF);
	######### End of SNP_FEATURE #########
}

## Read Samplesheet
## store in hash
#
sub parseSampleSheet{
	my ($samplesheet) = @_;
	my $linecount = 0;
	my $headerlines = 15;

	open(ST, "<$samplesheet") || die "Cannot open $samplesheet: $!";
	
	while(my $line = <ST> ){	
		chomp $line;
		if($linecount > $headerlines){
			my ($sample_id, $therest) = split(/\t/,$line,2);
			$stock{$sample_id} = [split(/\t/,$therest)];
		}
		$linecount++;
	}

	close(ST);
}

## read a tab-delimited file from GenomeStudio
## parse file 
## get genotypes, store in hash 
#
sub processGenomeStudio{
	my ($infile) = @_;
	open(IN,"<$infile") || die "Cannot open $infile:$!";
	
	my $linecount= 1; 
	while(my $line = <IN>){
		chomp $line;
		next if $line =~/^$/; #skip blank lines
		if($linecount == 10){ #skip all [Header] contents
			$line =~ s/^\s|\s$//g;
			@samples = split(/\t/,$line); # keeps sample identifiers
		}elsif($linecount > 10){
			my ($snp, $genotypes) = split(/\t/,$line,2);
			$snpMatrix{$snp} = [split(/\t/,$genotypes)];
		}
		$linecount++;
	}
	close(IN);

}

# print contents of Stock hash
sub printStocks{
	open(OUT,">","parseSampleSheet_output.tsv");
	foreach my $var1(keys %stock){
		print OUT $var1."\t"."$stock{$var1}[0]";
		#for my $x(0..$#{$stock{$var1}}){
		#	print "\t".$stock{$var1}[$x];
		#}
		print OUT "\n";
	}
	close(OUT);
}


## output hash values as json format
sub printOutputJSON{
	open(OUT, ">","processGS_outputJSON.txt");

	#initialize genotypesJSON hash
	foreach my $snp(sort keys %snpMatrix){
		foreach my $index(0..$#{$snpMatrix{$snp}}){
			my $allele = changeToIUPAC($snpMatrix{$snp}[$index]);
			#$genotypesJSON{$snp}{$samples[$index]}= $allele; 
			#$genotypesJSON{$snp}{$samples[$index]}[0] = $allele; 
			$genotypesJSON{$snp}{$samples[$index]}[0] = {'43611',0.00};  # cvtrem_id for infinium
			$genotypesJSON{$snp}{$samples[$index]}[1] = 0; ##rank
		}
	}

	#encode hash to json	
	foreach my $snp1(sort keys %genotypesJSON){
		my $json_genotype = encode_json( \%{$genotypesJSON{$snp1}} );
		print OUT $snp1."\t".$json_genotype."\n";
	}	
	
	##output hash contents
	##just to check if values properly loaded to hash

	#foreach my $snp1(sort keys %genotypesJSON){
	#	foreach my $sample(keys %{$genotypesJSON{$snp1}}){
	#		print OUT $snp1."\t".$sample."\t".$genotypesJSON{$snp1}{$sample}."\n";
	#		#print OUT $genotypesJSON{$snp1}{sample}."\n";
	#	}
	#}


	close(OUT);
}


## output hash values as Text Format
sub printOutputText{
	open(OUT, ">","processGS_outputText.txt");

	my @jsonArray;
	my $count = 1;
	
	#output as:
	foreach my $snp(sort keys %snpMatrix){
		#print OUT $snp."\t{";
		foreach my $x(0..$#{$snpMatrix{$snp}}){
			#change to IUPAC
			my $allele = changeToIUPAC($snpMatrix{$snp}[$x]);
			
			###############################################
			#Option1: print table as id|snp|sample|allele
			#
			print OUT "$count\t$snp\t$samples[$x]\t$allele\n";
			$count++;
			#
			#end of Option1
			###############################################
			
			###############################################
			#Option 2: print as \"sample\":\"allele\"
			#
			#print OUT "\\\"$samples[$x+1]\\\":\\\"$allele\\\",";			
			#
			#end of Option2
			################################################
	
			###test
			#my @array = ($samples[$x+1],$allele);
			#my $perl_scalar = encode_json(\@array);
			#print OUT $perl_scalar;
			##end_tes		
		}
		#$count++;
		#print OUT "}\n";
		#print OUT "\n";
	}
	close(OUT);
}

## change bi-allelic allele notation to IUPAC mono-allelic format
sub changeToIUPAC{
	my ($makeIUPAC) = @_;

	if($makeIUPAC eq "AA"){
		return "A";
	}elsif($makeIUPAC eq "CC"){
		return "C";
	}elsif($makeIUPAC eq "GG"){
		return "G";
	}elsif($makeIUPAC eq "TT"){
		return "T";
	}elsif($makeIUPAC eq "AG" or $makeIUPAC eq "GA"){
		return "R";
	}elsif($makeIUPAC eq "CT" or $makeIUPAC eq "TC"){
		return "Y";
	}elsif($makeIUPAC eq "CA" or $makeIUPAC eq "AC"){
		return "M";
	}elsif($makeIUPAC eq "TG" or $makeIUPAC eq "GT"){
		return "K";
	}elsif($makeIUPAC eq "TA" or $makeIUPAC eq "AT"){
		return "W";
	}elsif($makeIUPAC eq "CG" or $makeIUPAC eq "GC"){
		return "S";
	}elsif($makeIUPAC eq "--"){
		return "N";
	}else{
		return "N";
	}
}

############################ end of program ############################



