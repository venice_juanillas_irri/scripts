WORK_DIR=`pwd`
FASTQ_DIR=$WORK_DIR/"GBS_barcodeSplitter_fastq"
REFERENCE_DIR=$WORK_DIR/"reference"
OUTPUT_DIR=$WORK_DIR/"output_test"
SOFTWARE_DIR="/shared/data/software"

### tools:
PICARD=$SOFTWARE_DIR/"picard-tools-1.119"
GATK=$SOFTWARE_DIR/"GenomeAnalysisTK-3.2-2"

### Create Output Directory
mkdir $OUTPUT_DIR

### Reference Genome File
REFERENCE_FILENAME=$REFERENCE_DIR/"msu7_all.fa"

### Index genome using BWA
bwa index -a bwtsw $REFERENCE_FILENAME
java -jar $PICARD/CreateSequenceDictionary.jar R=$REFERENCE_FILENAME O=$REFERENCE_FILENAME."dict" #do this only once 

ALLREADS=`ls $FASTQ_DIR`

### do the next steps for all fastq files in the FASTQ_DIR
for READ in $ALLREADS
do
	READNAME=${READ%%.*}
	#echo $READNAME
	
	# 1.) Align to Reference
	#touch $READNAME_aligned.sai
	bwa aln -t 10 $REFERENCE_FILENAME $FASTQ_DIR/$READ > $OUTPUT_DIR/$READ."aligned.sai" 
	echo "Finished aligning $READNAME to reference."
	# 2.) Export Alignment format SAI to SAM format
        #touch $READNAME_aligned.sam
	bwa samse $REFERENCE_FILENAME $OUTPUT_DIR/$READ."aligned.sai" $FASTQ_DIR/$READNAME.fastq > $OUTPUT_DIR/$READ."aligned.sam" 
	echo "Finished exporting $READNAME alignment to SAM format."

	# Sort SAM
	java -jar $PICARD/SortSam.jar SO=coordinate INPUT=$OUTPUT_DIR/$READ."aligned.sam" OUTPUT=$OUTPUT_DIR/$READ."aligned_sortedSAM.bam" VALIDATION_STRINGENCY=LENIENT CREATE_INDEX=TRUE 
	echo "Finished sorting SAM. Expect output in BAM format "

	java -jar $PICARD/FixMateInformation.jar SO=coordinate INPUT=$OUTPUT_DIR/$READ."aligned_sortedSAM.bam" OUTPUT=$OUTPUT_DIR/$READ."aligned_sortedSAM_fixmate.bam" VALIDATION_STRINGENCY=LENIENT CREATE_INDEX=TRUE 
	echo "Finished Fix Mate Information "
	
	java -jar $PICARD/MarkDuplicates.jar INPUT=$OUTPUT_DIR/$READ."aligned_sortedSAM_fixmate.bam" OUTPUT=$OUTPUT_DIR/$READ."aligned_sortedSAM_fixmate_markdups.bam" METRICS_FILE=$OUTPUT_DIR/$READ."metrics" VALIDATION_STRINGENCY=LENIENT CREATE_INDEX=TRUE MAX_FILE_HANDLES_FOR_READ_ENDS_MAP=1000
	echo "Finished Mark Duplicates "

	java -jar $PICARD/AddOrReplaceReadGroups.jar INPUT=$OUTPUT_DIR/$READ."aligned_sortedSAM_fixmate_markdups.bam" OUTPUT=$OUTPUT_DIR/$READ."aligned_sortedSAM_fixmate_markdups_readgroup.bam" RGID=$READNAME LB=$READNAME PL='Illumina' SM=$READNAME CN=BGI RGPU=$READNAME VALIDATION_STRINGENCY=LENIENT CREATE_INDEX=TRUE
	echo "Finished Add or Replace Read Groups "
	
	java -jar $GATK/GenomeAnalysisTK.jar -T RealignerTargetCreator -nt 20 -R $REFERENCE_FILENAME -I $OUTPUT_DIR/$READ."aligned_sortedSAM_fixmate_markdups_readgroup.bam" -o $OUTPUT_DIR/$READ."aligned_sortedSAM_fixmate_markdups_readgroup_realntarget.intervals"
	echo "Finished Realigner Target Creator "

	java -jar $GATK/GenomeAnalysisTK.jar -T IndelRealigner -I $OUTPUT_DIR/$READ."aligned_sortedSAM_fixmate_markdups_readgroup.bam" -R $REFERENCE_FILENAME -targetIntervals $OUTPUT_DIR/$READ."aligned_sortedSAM_fixmate_markdups_readgroup_realntarget.intervals" -o $OUTPUT_DIR/$READ."aligned_sortedSAM_fixmate_markdups_readgroup_indelRealn.bam"
	echo "Finished Indel realigner"

done

#Create file with bam files
ls $OUTPUT_DIR/*indelRealn.bam > $OUTPUT_DIR/"bamList" 

#Merge all BAM after Indel ReAlign
samtools merge $OUTPUT_DIR/"C01LHABXX_1.merged.bam" -b $OUTPUT_DIR/"bamList"

#create index
samtools index $OUTPUT_DIR/"C01LHABXX_1.merged.bam"

# Call SNPs using GATK UnifiedGenotyper
java -jar $GATK/GenomeAnalysisTK.jar -T UnifiedGenotyper -nt 20 -I $OUTPUT_DIR/"C01LHABXX_1.merged.bam" -R $REFERENCE_FILENAME -o $OUTPUT_DIR/"C01LHABXX_1.all_sites.vcf.gz" -glm SNP -mbq 20 --genotyping_mode DISCOVERY -out_mode EMIT_ALL_SITES

### end of script
